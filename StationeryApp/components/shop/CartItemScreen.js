import React from 'react';
import {View, Text, Image, StyleSheet, Button, TouchableOpacity, ScrollView } from 'react-native';
import {HeaderButtons, Item} from 'react-navigation-header-buttons';

import HeaderButton from '../HeaderButton';

const CartItemScreen = props => {
    return (
        <View style = {{flex:1}} >
            <ScrollView>
        <View style = {styles.product}>
            <TouchableOpacity onPress={() => {
                console.log('OPen Image');
            }}  >
            <Image style={styles.image} source={{uri: props.image }} />
            </TouchableOpacity>
            <View style= {styles.details}>
                <Text style={styles.title} >{props.title}</Text>
                <Text style={styles.price} >Rs.{props.amount} </Text>
                <Text style={styles.title} >Quantity: {props.quantity}</Text>
            </View>
            
        </View>
        </ScrollView>
        <TouchableOpacity>
        <View style= {styles.bottom}>
                <Text style={styles.total}>Rs.{props.totalAmount}</Text>

                <View style= {styles.checkout}>
                <Text style={styles.total}>Checkout</Text>
                <HeaderButtons HeaderButtonComponent={HeaderButton}>
                    <Item
                        title='Search'
                        iconName='arrow-forward-outline'
                    />
                </HeaderButtons>
        </View>
        </View>
        </TouchableOpacity>
    </View>
    );
};

const styles = StyleSheet.create({
    product:{
        shadowColor : 'black',
        shadowOpacity: 0.26,
        shadowOffset: {width: 0,height: 2},
        shadowRadius: 6,
        elevation: 15,
        borderRadius: 10,
        backgroundColor: 'white',
        height: 125,
        marginVertical: 10,
        marginHorizontal: 10,
        paddingHorizontal: 10,
        alignItems: 'center',
        flexDirection: 'row'
    },
    total:{
        fontSize: 20,
        fontWeight: 'bold',
        padding : 10
    },
    checkout:{
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    
    bottom:{
        height: 50,
        marginTop: '118%',
        backgroundColor: '#4a148c',
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    image: {

        width: 80,
        height: 80
    },
    details: {
        height: 100,
        marginLeft: 15,
        marginTop: 25,
        marginBottom: '10%',
        flex : 1
    },
    title:{
        fontWeight: 'bold',
        fontSize: 20
    },
    price:{
        fontSize: 16,
        fontWeight: 'bold',
        marginTop: 10
    },
    actions:{
        width: 80,
        height: 30,
        marginTop: '28%',
        flexDirection: 'row',
        alignContent: 'flex-end',
        alignItems: 'flex-end',
        backgroundColor: 'black',
        borderRadius: 30,
        paddingHorizontal: 5,
        justifyContent: 'center',
        paddingHorizontal: 5
    },
    button:{
        width: '100%',
        height: '100%'
    }
});

export default CartItemScreen;