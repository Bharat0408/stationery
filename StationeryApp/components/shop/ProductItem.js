import React from 'react';
import {View, Text, Image, StyleSheet, Button, TouchableOpacity } from 'react-native';

const ProductItem = props => {
    return (
        
        <View style = {styles.product}>
            <TouchableOpacity
                onPress= {props.onViewDetail} >
            <Image style={styles.image} source={{uri: props.image }} />
            </TouchableOpacity>
            <View style= {styles.details}>
            <Text style={styles.title} >{props.title}</Text>
            <Text style={styles.price} >Our Price: Rs.{props.price.toFixed(2)} </Text>
            <Text style= {styles.mrp}>MRP: Rs.{props.mrp.toFixed(2)} </Text>
            <Text style= {styles.description}>Description {props.description}</Text>
            </View>
            <View>
            <Button color={'red'} title ="Add to cart" onPress={props.onAddToCart}/>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    product:{
        shadowColor : 'black',
        shadowOpacity: 0.26,
        shadowOffset: {width: 0,height: 2},
        shadowRadius: 6,
        elevation: 15,
        borderRadius: 10,
        backgroundColor: 'white',
        height: 150,
        marginVertical: 10,
        marginHorizontal: 10,
        paddingHorizontal: 10,
        alignItems: 'center',
        flexDirection: 'row'
    },
    
    image: {

        width: 80,
        height: 80
    },
    details: {
        height: 100,
        marginLeft: 15,
        marginTop: 0.2,
        marginBottom: '10%',
        flex : 1
    },
    title:{
        fontWeight: 'bold',
        fontSize: 20
    },
    price:{
        fontSize: 16,
        fontWeight: 'bold',
        marginTop: 10
    },
    mrp: {
        fontSize: 10,
        color: '#888',
        marginTop: 5
    },
    description:{
        fontSize : 14,
        color: '#888',
        marginTop: 8
    },
    actions:{
        width: 80,
        height: 30,
        marginTop: '28%',
        flexDirection: 'row',
        alignContent: 'flex-end',
        alignItems: 'flex-end',
        backgroundColor: 'black',
        borderRadius: 30,
        paddingHorizontal: 5,
        justifyContent: 'center',
        paddingHorizontal: 5
    },
    button:{
        width: '100%',
        height: '100%'
    }
});

export default ProductItem;