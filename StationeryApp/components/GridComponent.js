import React from 'react';
import { TouchableOpacity, View, Text, StyleSheet, Image } from "react-native";

const GridComponent = props => {
    return (
    
        <TouchableOpacity style = {styles.gridItem}
                onPress={props.onSelect} >
        
        <View style={styles.container} > 
    
        <Image style = {styles.image} source = {props.color} />
       
            <View>
                <Text style = {styles.title} numberOfLines={2}>{props.title}</Text>
            </View>
        
        </View>
        </TouchableOpacity>
        
    );
};

const styles = StyleSheet.create({
    container:{
        flex: 1,
        borderRadius: 10,
        shadowColor: 'black',
        shadowOpacity: 0.1,
        shadowOffset: {width: 0, height:2},
        shadowRadius: 5,
        elevation: 5,
        padding: 15,
        justifyContent: 'center',
        alignItems: 'center'
    },
    gridItem: {
        flex: 1,
        margin: 15,
        height : 120
    },
    title: {
        fontSize: 15,
        fontWeight: 'bold',
        textAlign: 'center'
    },
    image:{
        height: 70,
        width: 80,
        justifyContent: 'flex-end',
        alignItems: 'center'
    }
});

export default GridComponent;