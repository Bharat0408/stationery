import HomeCategory from '../models/homeCategory';
import Categories from '../models/Categories';
import Product from '../models/Products';
import CartItem from '../models/cart-item';

export const HOMECATEGORY = [
    new HomeCategory('c1', 'Office',require('../assets/Office.jpg') ),
    new HomeCategory('c2', 'School','white'),
    new HomeCategory('c3', 'Kids', require('../assets/Kids.jpg')),
    new HomeCategory('c4', 'Gifts', require('../assets/Gofts.jpg')),
    new HomeCategory('c5', 'Art', require('../assets/Art.jpg')),
    new HomeCategory('c6', 'Others', require('../assets/Bags.jpg'))
];

export const CATEGORIES = [
    new Categories('o1','c1', 'Files'),
    new Categories('o2','c1', 'Paper'),
    new Categories('o3','c1', 'Notebook'),
    new Categories('o4','c1', 'Pen'),
    new Categories('o5','c1', 'Stapler'),
    new Categories('o6','c1', 'Glue'),
    new Categories('s1','c2', 'Pens'),
    new Categories('s2','c2', 'Paper'),
    new Categories('s3','c2', 'Notebook'),
    new Categories('s4','c2', 'Scale'),
    new Categories('s5','c2', 'Stapler'),
    new Categories('s6','c2', 'Glue')
];

export const PRODUCTS = [
    new Product(
        'p1',
        's2',
        'u1',
        'Flat Files',
        'https://4.imimg.com/data4/WF/AC/MY-3465345/flat-files-500x500.jpg',
        'Flat Files',
        10,
        8
    ),
    new Product(
        'p2',
        'o2',
        'u1',
        'Flat Files',
        'https://4.imimg.com/data4/WF/AC/MY-3465345/flat-files-500x500.jpg',
        'Flat Flat',
        13,
        10
    ),
    new Product(
        'p3',
        'o1',
        'u1',
        'Flat Files',
        'https://4.imimg.com/data4/WF/AC/MY-3465345/flat-files-500x500.jpg',
        'Flat Files',
        10,
        8
    ),
    new Product(
        'p4',
        'o1',
        'u1',
        'Flat Files',
        'https://4.imimg.com/data4/WF/AC/MY-3465345/flat-files-500x500.jpg',
        'Flat Files',
        10,
        8
    ),
    new Product(
        'p5',
        'o1',
        'u1',
        'Flat Files',
        'https://4.imimg.com/data4/WF/AC/MY-3465345/flat-files-500x500.jpg',
        'Flat Files',
        12,
        9
    )
    ];