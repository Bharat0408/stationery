import React from 'react';
import { FlatList, SafeAreaView } from 'react-native';
import {useSelector, useDispatch } from 'react-redux';
import {HeaderButtons, Item} from 'react-navigation-header-buttons';

import Cart from './CartScreen';
import Search from './SearchScreen';

import ProductItem from '../components/shop/ProductItem';
import * as cartActions from '../store/actions/cart';
import HeaderButton from '../components/HeaderButton';

const ProductsScreen = props => {
    const products = useSelector(state => state.products.availableProducts);
    
    const dispatch = useDispatch();
    
    return(
        <SafeAreaView>
        <FlatList 
                data={products}
                keyExtractor={item => item.productId}
                renderItem={itemData => 
                <ProductItem 
                    image={itemData.item.imageUrl}
                    title = {itemData.item.title}
                    price = {itemData.item.price}
                    mrp = {itemData.item.mrp}
                    description = {itemData.item.description}
                    onViewDetail = {() => {
                        props.navigation.navigate('ProductDetails',
                           { 
                               productId: itemData.item.productId,
                               productTitle: itemData.item.title                           
                        } 
                        );
                    }}
                    onAddToCart = {() => {
                        dispatch(cartActions.addToCart(itemData.item));
                    }}
                    
                    /> } 
            />
        </SafeAreaView>
    );
};

ProductsScreen.navigationOptions = navData => {
    return {
    headerTitle : 'All Products',
    headerRight : <HeaderButtons HeaderButtonComponent={HeaderButton}>
    <Item 
        title='Search'
        iconName='ios-search'
        
        onPress={() => {
            navData.navigation.navigate('Search');
        }} 
       
        />
    <Item 
        title='Cart'
        iconName='ios-cart'
        
        onPress={() => {
            navData.navigation.navigate('Cart');
        }} 
        />
    
</HeaderButtons>
    };
};

export default ProductsScreen;
