import React from 'react';
import { StyleSheet, Text, View,Button } from 'react-native';
import {HeaderButtons, Item} from 'react-navigation-header-buttons';

import HeaderButton from '../components/HeaderButton';

const SearchScreen = props => {
    return(
        <View>
            <Text>Search Screen</Text>
        </View>
    );
};

SearchScreen.navigationOptions = {
    headerTitle: 'Search Products',
    headerStyle : {
        backgroundColor: '#4a148c'
    },
    
    headerRight : 
    <HeaderButtons HeaderButtonComponent={HeaderButton}>
    <Item 
            title='Search'
            iconName='ios-search'
            
            onPress={() => {
                console.log('OPen Search Bar');
            }} 
            />
</HeaderButtons>

};

export default SearchScreen;