import React from 'react';
import {FlatList, View } from 'react-native';
import {useSelector, useDispatch } from 'react-redux';
import {HeaderButtons, Item} from 'react-navigation-header-buttons';

import Search from './SearchScreen';

import HeaderButton from '../components/HeaderButton';
import CartItemScreen from '../components/shop/CartItemScreen';
import * as cartActions from '../store/actions/cart';

const CartScreen = props => {
    const cartTotalAmount = useSelector(state => state.cart.totalAmount );
    const cartItems = useSelector(state => {
        const transformedCartItems = [];
        for (const key in state.cart.items) {
            transformedCartItems.push({
                productId: key,
                productTitle: state.cart.items[key].productTitle,
                productPrice: state.cart.items[key].productPrice,
                quantity: state.cart.items[key].quantity,
                sum: state.cart.items[key].sum,
                imageUrl: state.cart.items[key].imageUrl
            });
        }
            return transformedCartItems;
    });

    return(
        
        <FlatList
            data= {cartItems}
            keyExtractor= {item => item.productId}
            renderItem={itemData => 
                <CartItemScreen
                    image={itemData.item.imageUrl}
                    title = {itemData.item.productTitle}
                    amount = {itemData.item.sum}
                    quantity= {itemData.item.quantity}
                    totalAmount = {itemData.item.sum}
                />}
        />
    );
    
};

CartScreen.navigationOptions = {
    headerTitle : 'Review Cart',
    headerStyle : {
        backgroundColor: '#4a148c'
    },
    headerRight :  
    <HeaderButtons HeaderButtonComponent={HeaderButton}>
        <Item 
            title='Search'
            iconName='ios-search'
            
            onPress={() => {
                navData.navigation.navigate('Search');
            }} 
           
            />
        
</HeaderButtons>

};

export default CartScreen;
