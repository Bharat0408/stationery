import React from 'react';
import { StyleSheet, Text, View, FlatList, Touchable, TouchableOpacity, ScrollView } from 'react-native';
import SafeAreaView, { SafeAreaProvider } from 'react-native-safe-area-view';

import { HeaderButtons, Item} from 'react-navigation-header-buttons';
import Cart from './CartScreen';
import Search from './SearchScreen';

import {HOMECATEGORY} from '../data/dummy'; 
import {dummyData} from '../data/Data'; 
import HomeCategory from '../models/homeCategory';
import Carousel from '../components/Carousel';
import GridComponent from '../components/GridComponent';
import HeaderButton from '../components/HeaderButton';

const HomeScreen = props => {

    const renderGridItem = itemData => {
        return(
            <GridComponent 
                title = { itemData.item.title }    
                color = {itemData.item.color}
                onSelect = {() => {
                    props.navigation.navigate({
                        routeName: 'Categories',
                        params: {
                            categoryId: itemData.item.id
                        }
                    });
                }}
            />
        );
    };

    return(
        <SafeAreaProvider>
        <SafeAreaView style= {styles.screen}>
        <ScrollView >
            
        <View>
            <Carousel data={dummyData} />
        </View>

        <View style= {styles.category}><Text style= {styles.text}>Shop by Category</Text></View>
        
       <View>
       
             <FlatList 
             data= {HOMECATEGORY}
             keyExtractor = {item => item.id}
             renderItem={renderGridItem}
             numColumns={3} />
       </View>
       </ScrollView>
     </SafeAreaView>
     </SafeAreaProvider>
    );
};

HomeScreen.navigationOptions = navData => {
    return {
    headerTite : 'Home',
    headerStyle : {
        backgroundColor: '#4a148c'
    },
   
    headerLeft:
        <HeaderButtons HeaderButtonComponent={HeaderButton}>
        <Item 
                title='Drawer'
                iconName='ios-menu'
                onPress={() => {
                    navData.navigation.toggleDrawer();
                }} 
                />
    </HeaderButtons>,
    headerRight : () =>
    <HeaderButtons HeaderButtonComponent={HeaderButton}>
        <Item 
            title='Search'
            iconName='ios-search'
            
            onPress={() => {
                navData.navigation.navigate('Search');
            }} 
           
            />
        <Item 
            title='Cart'
            iconName='ios-cart'
            
            onPress={() => {
                navData.navigation.navigate('Cart');
            }} 
            />
        
</HeaderButtons>
    };
};

const styles= StyleSheet.create({
    screen:{
        flex : 1,
        justifyContent: 'center',
        alignItems: 'center',
        alignContent:'center'
    },
    category:{
        justifyContent: 'center',
        alignItems: 'center',
        alignContent:'center'
    },
    text:{
        justifyContent: 'center',
        alignItems: 'center',
        alignContent:'center',
        fontSize: 24,
        padding : 5,
    }

});

export default HomeScreen;