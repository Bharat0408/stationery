import React from 'react';
import { StyleSheet, Text, View,Button, Image, ScrollView } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';

import { HeaderButtons, Item} from 'react-navigation-header-buttons';
import Cart from './CartScreen';
import Search from './SearchScreen';

import * as cartActions from '../store/actions/cart';
import HeaderButton from '../components/HeaderButton';

const ProductDetailsScreen = props => {

    const productId = props.navigation.getParam('productId');

    const dispatch = useDispatch();

    const selectedProduct = useSelector(state => 
        state.products.availableProducts.find(prod => prod.productId === productId ) );
        return(
        <ScrollView>
            <View style={styles.screen} >
                <Image style={styles.image} source={{uri: selectedProduct.imageUrl}} />
                <Text style={styles.title} >{selectedProduct.title}</Text>
                <Text style={styles.description}>{selectedProduct.description}</Text>
                <View style={styles.button}>
                <Button title='Add to Cart' onPress={() => { 
                    dispatch(cartActions.addToCart(selectedProduct));
                }}/>
                </View>
                <Text style={styles.price}>Our Price {selectedProduct.price.toFixed(2)}</Text>
                <Text style={styles.mrp}>M.R.P. {selectedProduct.mrp.toFixed(2)}</Text>
            </View>
        </ScrollView>
    );
};

ProductDetailsScreen.navigationOptions = navData => {
    return {
        headerTitle : navData.navigation.getParam('productTitle'),
        headerRight : <HeaderButtons HeaderButtonComponent={HeaderButton}>
    <Item 
        title='Search'
        iconName='ios-search'
        
        onPress={() => {
            navData.navigation.navigate('Search');
        }} 
       
        />
    <Item 
        title='Cart'
        iconName='ios-cart'
        
        onPress={() => {
            navData.navigation.navigate('Cart');
        }} 
        />
    
</HeaderButtons>
    };
}

const styles = StyleSheet.create({
    screen:{
        padding: 7
    },
    image:{
        width: '100%',
        height: 250,
        borderRadius: 10
    },
    title:{
        fontSize: 25,
        color: '#888',
        textAlign: 'center',
        marginVertical: 20
    },
    description:{       
        fontSize: 18,
        color: '#888',
        textAlign: 'center'
    },
    price:{
        fontSize: 25,
        color: '#888'
    },
    mrp:{
        fontSize: 14,
        color: '#888'
    },
    button:{
        alignItems: 'center'
    }
});

export default ProductDetailsScreen;