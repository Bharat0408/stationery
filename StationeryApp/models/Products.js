import ProductsScreen from "../screens/ProductsScreen";

class Product{
    constructor (productId, categoryId, ownerId, title, imageUrl, description, mrp, price) {
        this.productId = productId;
        this.categoryId= categoryId;
        this.ownerId = ownerId;
        this.title = title;
        this.imageUrl = imageUrl;
        this.description = description;
        this.mrp = mrp;
        this.price = price;
    }
}

export default Product;