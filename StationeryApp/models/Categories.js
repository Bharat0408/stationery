class OfficeCategories {
    constructor(id,categoryId, title ) {
        this.id = id;
        this.categoryId= categoryId;
        this.title = title;
    }
}

export default OfficeCategories;