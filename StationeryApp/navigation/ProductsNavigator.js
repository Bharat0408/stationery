import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import {createAppContainer} from 'react-navigation';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createDrawerNavigator } from 'react-navigation-drawer';
import {Ionicons} from '@expo/vector-icons';

import CategoriesScreen from '../screens/CategoriesScreen';
import HomeScreen from '../screens/HomeScreen';
import ProductDetailsScreen from '../screens/ProductDetailsScreen';
import ProductsScreen from '../screens/ProductsScreen';
import SearchScreen from '../screens/SearchScreen';
import CartScreen from '../screens/CartScreen';
import MyAccountScreen from '../screens/MyAccountScreen';
import MyOrdersScreen from '../screens/MyOrdersScreen';
import LogoutScreen from '../screens/LogoutScreen';

const ProductsNavigator = createStackNavigator({
    Home: HomeScreen,
    Categories: CategoriesScreen,
    Products: ProductsScreen,
    ProductDetails: ProductDetailsScreen,
    Cart: CartScreen,
    Search : SearchScreen
});



const MainDrawerNavigator = createDrawerNavigator({
    Home: ProductsNavigator,
    MyAccount : MyAccountScreen,
    MyOrders : MyOrdersScreen,
    Logout : LogoutScreen
});

export default createAppContainer(MainDrawerNavigator);