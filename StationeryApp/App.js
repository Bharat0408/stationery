import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { createStore, combineReducers } from 'redux';
import { Provider } from 'react-redux';
import { StyleSheet, Text, View, SafeAreaView } from 'react-native';

import ProductsNavigator from './navigation/ProductsNavigator';
import productsreducer from './store/reducers/productsreducer';
import { createStackNavigator } from 'react-navigation-stack';
import cartReducer from './store/reducers/cart';

const rootReducer = combineReducers({
  products: productsreducer,
  cart : cartReducer
});

const store = createStore(rootReducer);

export default function App() {
  return (
    <SafeAreaView style= {styles.screen}>
    
    <Provider store={store}>
      <ProductsNavigator /> 
    </Provider>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  screen:{
    flex : 1
}
});
